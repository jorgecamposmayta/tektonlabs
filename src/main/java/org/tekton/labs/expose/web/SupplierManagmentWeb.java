package org.tekton.labs.expose.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.tekton.labs.product.management.business.SupplierManagementService;
import org.tekton.labs.product.management.model.api.Supplier;

@RestController
@RequestMapping("/supplier/management")
public class SupplierManagmentWeb {

    @Autowired
    SupplierManagementService supplerManagementService;

    @PostMapping("/add")
    ResponseEntity<Supplier> create(@RequestBody Supplier supplier) {
        return new ResponseEntity<Supplier>(supplerManagementService.create(supplier), HttpStatus.CREATED);
    }
    @GetMapping("/get")
    ResponseEntity<Supplier> findById(@Param("id") Long id) {
        return new ResponseEntity<Supplier>(supplerManagementService.findById(id), HttpStatus.OK);

    }

}

