package org.tekton.labs.expose.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.tekton.labs.product.management.business.ProductCategoryManagementService;
import org.tekton.labs.product.management.model.api.ProductCategory;
import org.tekton.labs.product.management.validation.Validate;

@RestController
@RequestMapping("/product/category/management")
public class ProductCategoryManagmentWeb {

    @Autowired
    ProductCategoryManagementService productCategoryManagementService;

    @PostMapping("/add")
    ResponseEntity<ProductCategory> create(@RequestBody ProductCategory productCategory) {
        Validate.now(productCategory);
        return new ResponseEntity<ProductCategory>(productCategoryManagementService.create(productCategory), HttpStatus.CREATED);
    }
    @GetMapping("/get")
    ResponseEntity<ProductCategory> findById(@Param("id") Long id) {
        return new ResponseEntity<ProductCategory>(productCategoryManagementService.findById(id), HttpStatus.OK);
    }
}

