package org.tekton.labs.expose.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.tekton.labs.product.management.business.CountryManagementService;
import org.tekton.labs.product.management.model.api.Country;
import org.tekton.labs.product.management.validation.Validate;

import javax.validation.Valid;

@RestController
@RequestMapping("/country/management")
public class CountryManagmentWeb {

    @Autowired
    CountryManagementService countryManagementService;

    @PostMapping("/add")
    ResponseEntity<Country> create(@Valid @RequestBody Country country) {
        Validate.now(country);
        return new ResponseEntity<Country>(countryManagementService.create(country), HttpStatus.CREATED);
    }

    @GetMapping("/get")
    ResponseEntity<Country> findById(@Param("id") Long id) {
        return new ResponseEntity<Country>(countryManagementService.findById(id), HttpStatus.OK);
    }

}

