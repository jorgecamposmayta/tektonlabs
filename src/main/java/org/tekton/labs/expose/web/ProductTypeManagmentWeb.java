package org.tekton.labs.expose.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.tekton.labs.product.management.business.ProductTypeManagementService;
import org.tekton.labs.product.management.model.api.ProductType;
import org.tekton.labs.product.management.validation.Validate;

@RestController
@RequestMapping("/product/type/management")
public class ProductTypeManagmentWeb {

    @Autowired
    ProductTypeManagementService productTypeManagementService;

    @PostMapping("/add")
    ResponseEntity<ProductType> create(@RequestBody ProductType productType) {
        Validate.now(productType);
        return new ResponseEntity<ProductType>(productTypeManagementService.create(productType), HttpStatus.CREATED);
    }

    @GetMapping("/get")
    ResponseEntity<ProductType> findById(@Param("id") Long id) {
        return new ResponseEntity<ProductType>(productTypeManagementService.findById(id), HttpStatus.OK);
    }

}

