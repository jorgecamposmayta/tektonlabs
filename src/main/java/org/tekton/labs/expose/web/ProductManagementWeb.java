package org.tekton.labs.expose.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.tekton.labs.product.management.business.ProductManagementService;
import org.tekton.labs.product.management.model.api.*;
import org.tekton.labs.product.management.validation.Validate;

@RestController
@RequestMapping("/product/management")
public class ProductManagementWeb {

    @Autowired
    ProductManagementService productManagementService;
    @GetMapping ("/get")
    ResponseEntity<ProductApi> findById(@Param("id") Long id) {
        return new ResponseEntity<ProductApi>(productManagementService.findById(id), HttpStatus.OK);
    }
    @PostMapping("/add")
    ResponseEntity<ProductApi> create(@RequestBody ProductApi productApi) {
        Validate.now(productApi);
        ProductApi productApiBack = productManagementService.create(productApi);
        if(productApiBack.getCode()==null){
            return new ResponseEntity<ProductApi>(ProductApi.builder().build(), HttpStatus.PRECONDITION_FAILED);
        }
        return new ResponseEntity<ProductApi>(productApiBack, HttpStatus.OK);
    }

    @PutMapping("/put")
    ResponseEntity<ProductApi> update(@RequestBody ProductApi productApi) {
        ProductApi exist =productManagementService.findById(productApi.getCode());
        if(exist.getCode()!=null && exist.getCode()==productApi.getCode()){
            return new ResponseEntity<ProductApi>(productManagementService.update(productApi), HttpStatus.OK);
        }
        return new ResponseEntity<ProductApi>(ProductApi.builder().build(), HttpStatus.PRECONDITION_FAILED);
    }
}

