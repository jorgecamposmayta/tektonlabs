package org.tekton.labs.product.management.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.tekton.labs.product.management.model.entity.ProductCategoryEntity;

@Repository
public interface ProductCategoryRepository extends CrudRepository<ProductCategoryEntity, Long> {

}
