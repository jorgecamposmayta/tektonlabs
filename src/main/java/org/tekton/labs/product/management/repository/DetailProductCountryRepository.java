package org.tekton.labs.product.management.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.tekton.labs.product.management.model.entity.DetailProductCountryEntity;

@Repository
public interface DetailProductCountryRepository extends CrudRepository<DetailProductCountryEntity, Long> {

}
