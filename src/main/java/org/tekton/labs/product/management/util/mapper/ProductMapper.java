package org.tekton.labs.product.management.util.mapper;

import org.tekton.labs.product.management.model.api.*;
import org.tekton.labs.product.management.model.entity.ProductEntity;

import java.util.List;

public class ProductMapper {

    /**
     * Metodo constructor que no debe ser instanciado por ser una clase útil.
     */
    private ProductMapper() {
    }

    public static ProductApi buildProductApi(ProductEntity productEntity, List<DetailProductCountry> detailsBack) {
        return ProductApi.builder()
                .code(productEntity.getCode())
                .name(productEntity.getName())
                .description(productEntity.getDescription())
                .stock(productEntity.getStock())
                .rotation(productEntity.getRotation())
                .supplier(Supplier.builder()
                        .code(productEntity.getSupplierCode())
                        .build())
                .productType(ProductType.builder()
                        .code(productEntity.getProductTypeCode())
                        .build())
                .productCategory(ProductCategory.builder()
                        .code(productEntity.getProductCategoryCode())
                        .build())
                .details(detailsBack)
                .build();


    }

    public static ProductApi buildProductApiFull(ProductEntity productEntity, List<DetailProductCountry> detailsBack, List<Review> reviews, Supplier supplier, ProductCategory productCategory, ProductType productType) {
        return ProductApi.builder()
                .code(productEntity.getCode())
                .name(productEntity.getName())
                .description(productEntity.getDescription())
                .stock(productEntity.getStock())
                .rotation(productEntity.getRotation())
                .supplier(supplier)
                .productType(productType)
                .productCategory(productCategory)
                .details(detailsBack)
                .reviews(reviews)
                .build();
    }

    public static ProductEntity buildProductEntity(ProductApi productApi) {
        return new ProductEntity(   productApi.getCode(),
                                    productApi.getName(),
                                    productApi.getDescription(),
                                    productApi.getStock(),
                                    productApi.getRotation(),

                                    //TODO AGREGAR VALIDACIONES OBLIGATORIAS 3 OBJETOS
                                    productApi.getSupplier().getCode(),
                                    productApi.getProductType().getCode(),
                                    productApi.getProductCategory().getCode());
    }
}