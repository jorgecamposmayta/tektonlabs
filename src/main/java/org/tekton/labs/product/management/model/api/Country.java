package org.tekton.labs.product.management.model.api;

import lombok.Builder;
import lombok.Data;
import org.tekton.labs.product.management.tryvalidations.Currency;

import javax.validation.constraints.*;

@Data
@Builder
public class Country {

    private Long code;

    private String name;

    private String currency;

}
