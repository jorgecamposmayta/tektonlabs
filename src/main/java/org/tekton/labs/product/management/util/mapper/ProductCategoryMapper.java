package org.tekton.labs.product.management.util.mapper;

import org.tekton.labs.product.management.model.api.ProductCategory;
import org.tekton.labs.product.management.model.entity.ProductCategoryEntity;

public class ProductCategoryMapper {

    private ProductCategoryMapper() {
    }

    public static ProductCategory buildProductCategory(ProductCategoryEntity productCategoryEntity) {
        return ProductCategory.builder()
                .code(productCategoryEntity.getCode())
                .name(productCategoryEntity.getName())
                .build();
    }

    public static ProductCategoryEntity buildProductCategoryEntity(ProductCategory productCategory) {
        return new ProductCategoryEntity(productCategory.getCode(), productCategory.getName());
    }
}


