package org.tekton.labs.product.management.model.api;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.Positive;

@Data
@Builder
public class ProductApi {
    private Long code;
    private String name;
    private String description;
    private Integer stock;
    private BigDecimal rotation ;
    private List<Review> reviews;
    private Supplier supplier;
    private ProductType productType;
    private ProductCategory productCategory;
    private List<DetailProductCountry> details;

}
