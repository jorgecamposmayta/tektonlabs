package org.tekton.labs.product.management.model.entity;

import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@RequiredArgsConstructor
@Entity
@Table(name = "detail_product_country")
public class DetailProductCountryEntity {

    @Id
    @Column(name = "detail_product_country_code")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long code;
    @Column(name = "product_code")
    private Long productCode;
    @Column(name = "country_code")
    private Long countryCode;
    @Column(name = "manufacturing_cost")
    private BigDecimal manufacturingCost;
    @Column(name = "price_without_taxes")
    private BigDecimal priceWithoutTaxes;

    public DetailProductCountryEntity(Long code, Long productCode, Long countryCode, BigDecimal manufacturingCost, BigDecimal priceWithoutTaxes) {
        this.code = code;
        this.productCode = productCode;
        this.countryCode = countryCode;
        this.manufacturingCost = manufacturingCost;
        this.priceWithoutTaxes = priceWithoutTaxes;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public Long getProductCode() {
        return productCode;
    }

    public void setProductCode(Long productCode) {
        this.productCode = productCode;
    }

    public Long getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(Long countryCode) {
        this.countryCode = countryCode;
    }

    public BigDecimal getManufacturingCost() {
        return manufacturingCost;
    }

    public void setManufacturingCost(BigDecimal manufacturingCost) {
        this.manufacturingCost = manufacturingCost;
    }

    public BigDecimal getPriceWithoutTaxes() {
        return priceWithoutTaxes;
    }

    public void setPriceWithoutTaxes(BigDecimal priceWithoutTaxes) {
        this.priceWithoutTaxes = priceWithoutTaxes;
    }

    @Override
    public String toString() {
        return "DetailProducCountrytEntity{" +
                "code=" + code +
                ", productCode=" + productCode +
                ", countryCode=" + countryCode +
                ", manufacturingCost=" + manufacturingCost +
                ", priceWithoutTaxes=" + priceWithoutTaxes +
                '}';
    }
}
