package org.tekton.labs.product.management.model.entity;

import lombok.RequiredArgsConstructor;

import javax.persistence.*;

@RequiredArgsConstructor
@Entity
@Table(name = "product_type")
public class ProductTypeEntity {

    @Id
    @Column(name = "product_type_code")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long code;

    @Column(name = "product_type_name")
    private String name;

    public ProductTypeEntity(long code, String name) {
        this.code = code;
        this.name = name;
    }

    public ProductTypeEntity(String name) {
        this.name = name;
    }

    public long getCode() {
        return code;
    }

    public void setCode(long code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ProductTypeEntity{" +
                "code=" + code +
                ", name='" + name + '\'' +
                '}';
    }
}
