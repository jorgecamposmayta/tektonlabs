package org.tekton.labs.product.management.business.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.tekton.labs.product.management.business.CountryManagementService;
import org.tekton.labs.product.management.model.api.Country;
import org.tekton.labs.product.management.model.entity.CountryEntity;
import org.tekton.labs.product.management.repository.CountryRepository;
import org.tekton.labs.product.management.util.mapper.CountryMapper;

@Service
public class CountryManagementServiceImpl implements CountryManagementService {
    @Autowired
    CountryRepository countryRepository;

    @Override
    public Country create(Country country) {
        CountryEntity countryEntity= CountryMapper.buildCountryEntity(country);
        return CountryMapper.buildCountry(countryRepository.save(countryEntity));
    }

    @Override
    @Cacheable(value = "countrys", key = "#countryId")
    public Country  findById(Long countryId) {
        return CountryMapper.buildCountry(countryRepository.findById(countryId).get());
    }
}