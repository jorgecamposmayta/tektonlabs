package org.tekton.labs.product.management.util.mapper;

import org.tekton.labs.product.management.model.api.Supplier;
import org.tekton.labs.product.management.model.entity.SupplierEntity;

public class SupplierMapper {

    private SupplierMapper() {
    }

    public static Supplier buildSupplier(SupplierEntity supplierEntity) {
        return Supplier.builder()
                .code(supplierEntity.getCode())
                .name(supplierEntity.getName())
                .build();
    }

    public static SupplierEntity buildSupplierEntity(Supplier supplier) {
        return new SupplierEntity(supplier.getCode(), supplier.getName());
    }
}


