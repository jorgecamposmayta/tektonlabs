package org.tekton.labs.product.management.model.api;

import lombok.Data;

import java.io.Serializable;

@Data
public class Review implements Serializable {
    private String productCode;
    private String customerName;
    private String opinion;
    private Integer stars;

    public Review() {
    }
}
