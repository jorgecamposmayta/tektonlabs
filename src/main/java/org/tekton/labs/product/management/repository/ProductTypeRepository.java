package org.tekton.labs.product.management.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.tekton.labs.product.management.model.entity.ProductTypeEntity;

@Repository
public interface ProductTypeRepository extends CrudRepository<ProductTypeEntity, Long> {

}
