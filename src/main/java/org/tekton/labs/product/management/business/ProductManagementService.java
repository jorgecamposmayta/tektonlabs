package org.tekton.labs.product.management.business;

import org.tekton.labs.product.management.model.api.DetailProductCountry;
import org.tekton.labs.product.management.model.api.ProductApi;

public interface ProductManagementService {

    ProductApi create(ProductApi productApi);
    DetailProductCountry createDetail(DetailProductCountry detailProducCountry);
    ProductApi update(ProductApi productApi);
    DetailProductCountry createDetailUpdate(DetailProductCountry detailProducCountry);
    ProductApi  findById(Long id);
}
