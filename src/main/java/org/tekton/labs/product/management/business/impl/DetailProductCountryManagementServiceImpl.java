package org.tekton.labs.product.management.business.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tekton.labs.product.management.business.DetailProductCountryManagementService;
import org.tekton.labs.product.management.model.api.DetailProductCountry;
import org.tekton.labs.product.management.model.entity.DetailProductCountryEntity;
import org.tekton.labs.product.management.repository.DetailProductCountryRepository;
import org.tekton.labs.product.management.util.mapper.DetailProductCountryMapper;

@Service
public class DetailProductCountryManagementServiceImpl implements DetailProductCountryManagementService {

    @Autowired
    DetailProductCountryRepository detailProducCountryRepository;

    @Override
    public DetailProductCountry create(DetailProductCountry detailProductCountry) {
        DetailProductCountryEntity detailProductCountrytEntityEntity= DetailProductCountryMapper.buildDetailProductCountryEntity(detailProductCountry);
        return DetailProductCountryMapper.buildDetailProductCountry(detailProducCountryRepository.save(detailProductCountrytEntityEntity));
    }
}
