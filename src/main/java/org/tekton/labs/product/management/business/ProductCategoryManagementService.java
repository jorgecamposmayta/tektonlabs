package org.tekton.labs.product.management.business;

import org.springframework.cache.annotation.Cacheable;
import org.tekton.labs.product.management.model.api.ProductCategory;

public interface ProductCategoryManagementService {
    ProductCategory create(ProductCategory productCategory);
    @Cacheable(value = "productCategorys", key = "#productCategoryId")
    ProductCategory  findById(Long productCategoryId);
}
