package org.tekton.labs.product.management.business.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.tekton.labs.product.management.business.SupplierManagementService;
import org.tekton.labs.product.management.model.api.Supplier;
import org.tekton.labs.product.management.model.entity.SupplierEntity;
import org.tekton.labs.product.management.repository.SupplierRepository;
import org.tekton.labs.product.management.util.mapper.SupplierMapper;

@Service
public class SupplierManagementServiceImpl implements SupplierManagementService {

    @Autowired
    SupplierRepository supplierRepository;

    public SupplierManagementServiceImpl() {
    }

    @Override
    public Supplier create(Supplier supplier) {
        SupplierEntity supplierEntity=SupplierMapper.buildSupplierEntity(supplier);
        return SupplierMapper.buildSupplier(supplierRepository.save(supplierEntity));
    }

    @Override
    @Cacheable(value = "suppliers", key = "#supplierId")
    public Supplier  findById(Long supplierId) {
        return SupplierMapper.buildSupplier(supplierRepository.findById(supplierId).get());
    }
}