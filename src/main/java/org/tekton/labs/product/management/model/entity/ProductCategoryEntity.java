package org.tekton.labs.product.management.model.entity;

import lombok.RequiredArgsConstructor;

import javax.persistence.*;

@RequiredArgsConstructor
@Entity
@Table(name = "product_category")
public class ProductCategoryEntity {

    @Id
    @Column(name = "product_category_code")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long code;

    @Column(name = "product_category_name")
    private String name;

    public ProductCategoryEntity(long code, String name) {
        this.code = code;
        this.name = name;
    }

    public ProductCategoryEntity(String name) {
        this.name = name;
    }

    public long getCode() {
        return code;
    }

    public void setCode(long code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ProductCategoryEntity{" +
                "code=" + code +
                ", name='" + name + '\'' +
                '}';
    }
}
