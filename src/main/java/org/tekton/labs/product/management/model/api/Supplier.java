package org.tekton.labs.product.management.model.api;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Supplier {
    private Long code;
    private String name;

}
