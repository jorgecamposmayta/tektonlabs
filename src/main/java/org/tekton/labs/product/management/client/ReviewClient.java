package org.tekton.labs.product.management.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.tekton.labs.product.management.model.api.Review;

import java.util.Arrays;
import java.util.List;

@Service
public class ReviewClient {
    @Autowired(required = true)
    @Value("${service.external.url}")
    private String apiUrl;
    @Autowired
    private RestTemplate restTemplate;

    public List<Review> getReviews(String productCode) {
        ResponseEntity<Review[]> response = restTemplate.getForEntity(apiUrl+"="+productCode, Review[].class);
        return Arrays.asList(response.getBody());
    }
}
