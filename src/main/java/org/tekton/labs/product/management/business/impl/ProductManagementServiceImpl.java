package org.tekton.labs.product.management.business.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tekton.labs.product.management.business.CountryManagementService;
import org.tekton.labs.product.management.business.ProductManagementService;
import org.tekton.labs.product.management.client.ReviewClient;
import org.tekton.labs.product.management.model.api.*;
import org.tekton.labs.product.management.model.entity.DetailProductCountryEntity;
import org.tekton.labs.product.management.model.entity.ProductEntity;
import org.tekton.labs.product.management.repository.DetailProductCountryJPARepository;
import org.tekton.labs.product.management.repository.DetailProductCountryRepository;
import org.tekton.labs.product.management.repository.ProductRepository;
import org.tekton.labs.product.management.util.mapper.DetailProductCountryMapper;
import org.tekton.labs.product.management.util.mapper.ProductMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductManagementServiceImpl implements ProductManagementService {

    @Autowired
    ProductRepository productRepository;
    @Autowired
    DetailProductCountryRepository detailProducCountryRepository;

    @Autowired
    DetailProductCountryJPARepository detailProducCountryJPARepository;

    @Autowired
    SupplierManagementServiceImpl supplierManagementService;
    @Autowired
    ProductCategoryManagementServiceImpl productCategoryManagementService;
    @Autowired
    ProductTypeManagementServiceImpl productTypeManagementService;

    @Autowired
    CountryManagementService countryManagementService;

    @Autowired
    DetailProductCountryRepository detailProductCountryRepository;

    @Autowired
    ReviewClient reviewClient;

    public ProductApi update(ProductApi productApi) {

        /* actualizar maestro*/
        ProductEntity productEntityBack= productRepository.save(ProductMapper.buildProductEntity(productApi));
        List<DetailProductCountry> detailsBack = new ArrayList<>();

        /* actualizar detalles con el cod producto enviado (es una actualizacion!)*/
        productApi.getDetails().forEach(detail -> {
            detail.setProductCode(productEntityBack.getCode());
            detailsBack.add(createDetailUpdate(detail));
        });

        return ProductMapper.buildProductApi(productEntityBack,detailsBack);
    }

    public DetailProductCountry createDetailUpdate(DetailProductCountry detailProductCountry) {
        DetailProductCountryEntity detailProductCountryEntityEntity= DetailProductCountryMapper.buildDetailProductCountryEntity(detailProductCountry);
        return DetailProductCountryMapper.buildDetailProductCountry(detailProducCountryRepository.save(detailProductCountryEntityEntity));
    }


    @Override
    public ProductApi create(ProductApi productApi) {
        /* Si existe producto con el cod de producto solicitado, no corresponde crearlo */
        if(productRepository.findById(productApi.getCode()).isPresent()){
            return ProductApi.builder().build();
        };
        /* guardar en tabla maestro */
        ProductEntity productEntityBack= productRepository.save(ProductMapper.buildProductEntity(productApi));
        List<DetailProductCountry> detailsBack = new ArrayList<>();

        /* guardar detalles con el codigo de producto regitrado previamente*/
        productApi.getDetails().forEach(detail -> {
            detail.setProductCode(productEntityBack.getCode());
            detailsBack.add(createDetail(detail));
        });

        return ProductMapper.buildProductApi(productEntityBack,detailsBack);
    }

    @Override
    public DetailProductCountry createDetail(DetailProductCountry detailProductCountry) {
        DetailProductCountryEntity detailProductCountryEntityEntity= DetailProductCountryMapper.buildDetailProductCountryEntity(detailProductCountry);
        detailProductCountryEntityEntity.setCode(detailProductCountryRepository.count()+1);
        return DetailProductCountryMapper.buildDetailProductCountry(detailProducCountryRepository.save(detailProductCountryEntityEntity));
    }

    @Override
    public ProductApi  findById(Long id) {
        Optional<ProductEntity> productEntity= productRepository.findById(id);

        List<DetailProductCountryEntity> detailsEntity = new ArrayList<>();
        List<DetailProductCountry> details = new ArrayList<>();
        List<Review> reviews= new ArrayList<>();
        ProductApi productApiBack = ProductApi.builder().build();

        if (productEntity.isPresent()){
            /* invocar servicios externo de reseñas */
            reviews=reviewClient.getReviews(productEntity.get().getCode().toString());

            /* traer lista de detalles, recorrer, extraer codigo de producto */
            detailProducCountryJPARepository.findAllByProductCode(productEntity.get().getCode()).forEach( detailEntity -> {
                Country country = countryManagementService.findById(detailEntity.getCountryCode()); // Traer pais de cache por cada detalle
                /* agregar cada detalle a la lista de  detalle */
                details.add(DetailProductCountryMapper.buildDetailProducCountryFull(detailEntity , country.getName(), country.getCurrency(), country));
            });

            /* Completar datos desde la memoria cache en caso no existan */
            Supplier supplier = supplierManagementService.findById(productEntity.get().getSupplierCode());
            ProductCategory productCategory = productCategoryManagementService.findById(productEntity.get().getProductCategoryCode());
            ProductType productType = productTypeManagementService.findById(productEntity.get().getProductCategoryCode());
            productApiBack = ProductMapper.buildProductApiFull(productEntity.get(),details, reviews, supplier, productCategory, productType);
        };

        return productApiBack;
    }
}
