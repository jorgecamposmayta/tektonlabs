package org.tekton.labs.product.management.util.mapper;

import org.tekton.labs.product.management.model.api.Country;
import org.tekton.labs.product.management.model.entity.CountryEntity;

public class CountryMapper {

    private CountryMapper() {
    }

    public static Country buildCountry(CountryEntity countryEntity) {
        return Country.builder()
                .code(countryEntity.getCode())
                .name(countryEntity.getName())
                .currency(countryEntity.getCurrency())
                .build();
    }

    public static CountryEntity buildCountryEntity(Country country) {
        return new CountryEntity(country.getCode(), country.getName(), country.getCurrency());
    }
}


