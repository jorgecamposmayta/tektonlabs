package org.tekton.labs.product.management.tryvalidations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CurrencyValidator implements ConstraintValidator<Currency, String> {

    private static final String CURRENCY_PATTERN = "^[A-Z]{3}$"; // Ejemplo: USD, EUR, etc.

    @Override
    public void initialize(Currency constraintAnnotation) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return validateCurrency(value);
    }

    private boolean validateCurrency(String currency) {
        if (currency == null) {
            return false;
        }

        Pattern pattern = Pattern.compile(CURRENCY_PATTERN);
        Matcher matcher = pattern.matcher(currency);

        return matcher.matches();
    }
}
