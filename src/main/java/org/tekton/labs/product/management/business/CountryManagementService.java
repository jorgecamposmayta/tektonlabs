package org.tekton.labs.product.management.business;

import org.springframework.cache.annotation.Cacheable;
import org.tekton.labs.product.management.model.api.Country;

public interface CountryManagementService {
    Country create(Country country);
    @Cacheable(value = "countrys", key = "#countryId")
    Country  findById(Long countryId);
}
