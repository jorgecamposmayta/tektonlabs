package org.tekton.labs.product.management.validation;

import org.tekton.labs.product.management.model.api.*;

import static org.tekton.labs.product.management.validation.Valid.*;

public class Validate {

    private static final String MENSAJE_NO_NULO="El valor no puede ser nulo";

    private static final String MENSAJE_ALFANUMERICO_1_255="Ingresar valor alfanumerico, longitud 1-255 caracteres";
    private static final String REG_EXPRESION_ALFANUMERICO_1_255="^[[\\p{Print}&&[^']&&[^\\\\]]]{1,255}$";

    private static final String MENSAJE_ALFANUMERICO_1_100="Ingresar valor alfanumerico, longitud 1-100 caracteres";
    private static final String REG_EXPRESION_ALFANUMERICO_1_100="^[[\\p{Print}&&[^']&&[^\\\\]]]{1,100}$";



    private static final String MENSAJE_LETRAS_1_50="Ingresar solo letras, longitud 1-50 caracteres";
    private static final String REG_EXPRESION_LETRAS_1_50="^[a-zA-Z]{1,50}$";


    private static final String MENSAJE_LETRAS_MAYUSCULAS_3 ="ingresar solo letras MAYUSCULAS, longitud = 3 caracteres";
    private static final String REG_EXPRESION_LETRAS_MAYUSCULAS_3 ="^[A-Z]{3,3}$";


    private static final String MENSAJE_NUMEROS_ENTEROS_POSITIVO ="ingresar solo numeros enteros positivos";
    private static final String MENSAJE_NUMEROS_DECIMALES_POSITIVO ="ingresar solo numeros decimales  positivos";
    private static final String REG_EXPRESION_NUMEROS_ENTEROS="^-?\\d+$";

    public static void now(ProductApi object){
        validateNull(object.getDescription(), "Descripcion: "+MENSAJE_NO_NULO);
        validateString(REG_EXPRESION_ALFANUMERICO_1_255, object.getDescription(), "Descripcion: "+MENSAJE_ALFANUMERICO_1_255);

        validateNull(object.getName(), "Name: "+MENSAJE_NO_NULO);
        validateString(REG_EXPRESION_ALFANUMERICO_1_100, object.getName(), "Name: "+REG_EXPRESION_ALFANUMERICO_1_100);

        validateNull(object.getStock(), "Stock: "+MENSAJE_NO_NULO);
        validatePositive( object.getStock(), "Stock: "+ MENSAJE_NUMEROS_ENTEROS_POSITIVO);

        validateNull(object.getRotation(), "Rotation: "+MENSAJE_NO_NULO);
        validateDecimalPositive( object.getRotation(), "Rotation: "+ MENSAJE_NUMEROS_DECIMALES_POSITIVO);

        validateNull(object.getSupplier(), "Supplier: "+MENSAJE_NO_NULO);
        validateNull(object.getSupplier().getCode(), "Supplier.Code: "+MENSAJE_NO_NULO);

        validateNull(object.getProductCategory(), "ProductCategory: "+MENSAJE_NO_NULO);
        validateNull(object.getProductCategory().getCode(), "ProductCategory.Code: "+MENSAJE_NO_NULO);

        validateNull(object.getProductType(), "ProductType: "+MENSAJE_NO_NULO);
        validateNull(object.getProductType().getCode(), "ProductType.Code: "+MENSAJE_NO_NULO);

        validateNull(object.getDetails(), "Details: "+MENSAJE_NO_NULO);
        object.getDetails().forEach( details ->{
            validateNull(details.getCountry(), "Details.Country: "+MENSAJE_NO_NULO);
            validateNull(details.getCountry().getCode(), "Details.Country.Code: "+MENSAJE_NO_NULO);
            validateNull(details.getCountry().getCurrency(), "Details.Country.Currency: "+MENSAJE_NO_NULO);
            validateString(REG_EXPRESION_LETRAS_MAYUSCULAS_3, details.getCountry().getCurrency(), "Details.Country.Currency: "+MENSAJE_LETRAS_MAYUSCULAS_3);

            validateNull(details.getManufacturingCost(), "Details.ManufacturingCost: "+MENSAJE_NO_NULO);
            validateDecimalPositive( details.getManufacturingCost(), "Details.ManufacturingCost: "+ MENSAJE_NUMEROS_DECIMALES_POSITIVO);

            validateNull(details.getPriceWithoutTaxes(), "Details.PriceWithoutTaxes: "+MENSAJE_NO_NULO);
            validateDecimalPositive( details.getPriceWithoutTaxes(), "Details.PriceWithoutTaxes: "+ MENSAJE_NUMEROS_DECIMALES_POSITIVO);
        });


    }

    public static void now(Country object){
        validateString(REG_EXPRESION_LETRAS_MAYUSCULAS_3, object.getCurrency(), "Currency: "+MENSAJE_LETRAS_MAYUSCULAS_3);

        validateNull(object.getName(), "Name: "+MENSAJE_NO_NULO);
        validateString(REG_EXPRESION_LETRAS_1_50, object.getName(), "Name: "+MENSAJE_LETRAS_1_50);
    }

    public static void now(ProductType object){
        validateNull(object.getName(), "Name: "+MENSAJE_NO_NULO);
        validateString(REG_EXPRESION_LETRAS_1_50, object.getName(), "Name: "+MENSAJE_LETRAS_1_50 );
    }

    public static void now(ProductCategory object){
        validateNull(object.getName(), "Name: "+MENSAJE_NO_NULO);
        validateString(REG_EXPRESION_LETRAS_1_50, object.getName(), "Name: "+MENSAJE_LETRAS_1_50 );
    }


}
