package org.tekton.labs.product.management.util.mapper;

import org.tekton.labs.product.management.model.api.*;
import org.tekton.labs.product.management.model.entity.DetailProductCountryEntity;

public class DetailProductCountryMapper {

    private DetailProductCountryMapper() {
    }

    public static DetailProductCountry buildDetailProductCountry(DetailProductCountryEntity detailProductCountryEntity) {
        return DetailProductCountry.builder()
                .code(detailProductCountryEntity.getCode())
                .productCode(detailProductCountryEntity.getProductCode())
                .country(Country.builder()
                        .code(detailProductCountryEntity.getCode())
                        .build())
                .manufacturingCost(detailProductCountryEntity.getManufacturingCost())
                .priceWithoutTaxes(detailProductCountryEntity.getPriceWithoutTaxes())
                .build();
    }

    public static DetailProductCountry buildDetailProducCountryFull(DetailProductCountryEntity detailProductCountryEntity, String name, String currency, Country country) {
        return DetailProductCountry.builder()
                .code(detailProductCountryEntity.getCode())
                .productCode(detailProductCountryEntity.getProductCode())
                .country(country)
                .manufacturingCost(detailProductCountryEntity.getManufacturingCost())
                .priceWithoutTaxes(detailProductCountryEntity.getPriceWithoutTaxes())
                .build();
    }

    public static DetailProductCountryEntity buildDetailProductCountryEntity(DetailProductCountry detailProductCountry) {
        return new DetailProductCountryEntity(  detailProductCountry.getCode(),
                                                detailProductCountry.getProductCode(),
                                                detailProductCountry.getCountry().getCode(),
                                                detailProductCountry.getManufacturingCost(),
                                                detailProductCountry.getPriceWithoutTaxes());
    }
}