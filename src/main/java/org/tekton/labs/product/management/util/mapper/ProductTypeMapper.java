package org.tekton.labs.product.management.util.mapper;

import org.tekton.labs.product.management.model.api.ProductType;
import org.tekton.labs.product.management.model.entity.ProductTypeEntity;

public class ProductTypeMapper {

    private ProductTypeMapper() {
    }

    public static ProductType buildProductType(ProductTypeEntity productTypeEntity) {
        return ProductType.builder()
                .code(productTypeEntity.getCode())
                .name(productTypeEntity.getName())
                .build();
    }

    public static ProductTypeEntity buildProductTypeEntity(ProductType productType) {
        return new ProductTypeEntity(productType.getCode(), productType.getName());
    }
}


