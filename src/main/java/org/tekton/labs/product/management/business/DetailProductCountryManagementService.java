package org.tekton.labs.product.management.business;

import org.tekton.labs.product.management.model.api.DetailProductCountry;

public interface DetailProductCountryManagementService {
    DetailProductCountry create(DetailProductCountry detailProductCountry);
}
