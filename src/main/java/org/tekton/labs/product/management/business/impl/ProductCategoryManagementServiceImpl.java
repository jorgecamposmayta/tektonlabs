package org.tekton.labs.product.management.business.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.tekton.labs.product.management.business.ProductCategoryManagementService;
import org.tekton.labs.product.management.model.api.ProductCategory;
import org.tekton.labs.product.management.model.entity.ProductCategoryEntity;
import org.tekton.labs.product.management.repository.ProductCategoryRepository;
import org.tekton.labs.product.management.util.mapper.ProductCategoryMapper;

@Service
public class ProductCategoryManagementServiceImpl implements ProductCategoryManagementService {

    @Autowired
    ProductCategoryRepository productCategoryRepository;

    @Override
    public ProductCategory create(ProductCategory productCategory) {
        ProductCategoryEntity productCategoryEntity= ProductCategoryMapper.buildProductCategoryEntity(productCategory);
        return ProductCategoryMapper.buildProductCategory(productCategoryRepository.save(productCategoryEntity));
    }

    @Override
    @Cacheable(value = "productCategorys", key = "#productCategoryId")
    public ProductCategory  findById(Long productCategoryId) {
        return ProductCategoryMapper.buildProductCategory(productCategoryRepository.findById(productCategoryId).get());
    }

}