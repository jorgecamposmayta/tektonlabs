package org.tekton.labs.product.management.business;

import org.springframework.cache.annotation.Cacheable;
import org.tekton.labs.product.management.model.api.Supplier;

public interface SupplierManagementService {
    Supplier create(Supplier Supplier);
    @Cacheable(value = "suppliers", key = "#supplierId")
    Supplier  findById(Long supplierId);
}
