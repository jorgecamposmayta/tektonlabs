package org.tekton.labs.product.management.config;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.logging.Logger;

@Component
public class TimingInterceptor implements HandlerInterceptor {

    private static final Logger logger = Logger.getLogger(TimingInterceptor.class.getName());

    private long startTime;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        startTime = System.currentTimeMillis();
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
        long elapsedTime = System.currentTimeMillis() - startTime;
        String logMessage = String.format("Tiempo de respuesta para %s: %d ms", request.getRequestURI(), elapsedTime);
        logger.info(logMessage);
    }
}
