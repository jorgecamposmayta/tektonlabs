package org.tekton.labs.product.management.model.entity;

import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@RequiredArgsConstructor
@Entity
@Table(name = "product")
public class ProductEntity {

    @Id
    @Column(name = "product_code")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long code;

    @Column(name = "product_name")
    private String name;
    @Column(name = "product_description")
    private String description;
    @Column(name = "stock")
    private Integer stock;
    @Column(name = "rotation")
    private BigDecimal rotation;
    @Column(name = "supplier_code")
    private Long supplierCode;
    @Column(name = "product_type_code")
    private Long productTypeCode;
    @Column(name = "product_category_code")
    private Long productCategoryCode;

    public ProductEntity(Long code, String name, String description, Integer stock, BigDecimal rotation, Long supplierCode, Long productTypeCode, Long productCategoryCode) {
        this.code = code;
        this.name = name;
        this.description = description;
        this.stock = stock;
        this.rotation = rotation;
        this.supplierCode = supplierCode;
        this.productTypeCode = productTypeCode;
        this.productCategoryCode = productCategoryCode;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public BigDecimal getRotation() {
        return rotation;
    }

    public void setRotation(BigDecimal rotation) {
        this.rotation = rotation;
    }

    public Long getSupplierCode() {
        return supplierCode;
    }

    public void setSupplierCode(Long supplierCode) {
        this.supplierCode = supplierCode;
    }

    public Long getProductTypeCode() {
        return productTypeCode;
    }

    public void setProductTypeCode(Long productTypeCode) {
        this.productTypeCode = productTypeCode;
    }

    public Long getProductCategoryCode() {
        return productCategoryCode;
    }

    public void setProductCategoryCode(Long productCategoryCode) {
        this.productCategoryCode = productCategoryCode;
    }

    @Override
    public String toString() {
        return "ProductEntity{" +
                "code=" + code +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", stock=" + stock +
                ", rotation=" + rotation +
                ", supplierCode=" + supplierCode +
                ", productTypeCode=" + productTypeCode +
                ", productCategoryCode=" + productCategoryCode +
                '}';
    }
}
