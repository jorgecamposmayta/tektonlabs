package org.tekton.labs.product.management.model.api;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Builder
public class ProductType {

    private Long code;

    private String name;
}
