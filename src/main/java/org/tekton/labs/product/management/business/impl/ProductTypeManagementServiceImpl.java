package org.tekton.labs.product.management.business.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.tekton.labs.product.management.business.ProductTypeManagementService;
import org.tekton.labs.product.management.model.api.ProductType;
import org.tekton.labs.product.management.model.entity.ProductTypeEntity;
import org.tekton.labs.product.management.repository.ProductTypeRepository;
import org.tekton.labs.product.management.util.mapper.ProductTypeMapper;

@Service
public class ProductTypeManagementServiceImpl implements ProductTypeManagementService {

    @Autowired
    ProductTypeRepository productTypeRepository;

    @Override
    public ProductType create(ProductType productType) {
        ProductTypeEntity productTypeEntity= ProductTypeMapper.buildProductTypeEntity(productType);
        return ProductTypeMapper.buildProductType(productTypeRepository.save(productTypeEntity));
    }

    @Override
    @Cacheable(value = "productTypes", key = "#productTypeId")
    public ProductType  findById(Long productTypeId) {
        return ProductTypeMapper.buildProductType(productTypeRepository.findById(productTypeId).get());
    }

}