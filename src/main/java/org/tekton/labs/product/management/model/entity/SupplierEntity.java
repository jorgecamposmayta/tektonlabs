package org.tekton.labs.product.management.model.entity;

import lombok.RequiredArgsConstructor;

import javax.persistence.*;

@RequiredArgsConstructor
@Entity
@Table(name = "supplier")
public class SupplierEntity {

    @Id
    @Column(name = "supplier_code")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long code;

    @Column(name = "supplier_name")
    private String name;

    public SupplierEntity(long code, String name) {
        this.code = code;
        this.name = name;
    }

    public SupplierEntity(String name) {
        this.name = name;
    }

    public long getCode() {
        return code;
    }

    public void setCode(long code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "SupplierEntity{" +
                "code=" + code +
                ", name='" + name + '\'' +
                '}';
    }
}
