package org.tekton.labs.product.management.model.entity;

import lombok.RequiredArgsConstructor;

import javax.persistence.*;

@RequiredArgsConstructor
@Entity
@Table(name = "country")
public class CountryEntity {

    @Id
    @Column(name = "country_code")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long code;

    @Column(name = "country_name")
    private String name;

    @Column(name = "currency")
    private String currency;

    public CountryEntity(Long code, String name, String currency) {
        this.code = code;
        this.name = name;
        this.currency = currency;
    }

    public CountryEntity(String name) {
        this.name = name;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return "CountryEntity{" +
                "code=" + code +
                ", name='" + name + '\'' +
                ", currency='" + currency + '\'' +
                '}';
    }
}
