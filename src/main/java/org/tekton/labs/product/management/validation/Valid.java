package org.tekton.labs.product.management.validation;

import java.math.BigDecimal;

public class Valid {
    public static void validateString(String rexg, String evaluar, String mensaje){
        boolean valid =evaluar.matches(rexg);
        if (!valid) {
            System.out.println("no valido");
            throw new BadRequestException(mensaje+" // no cumple con la expression regular: "+rexg);
        }
    }

    public static void validatePositive( Integer evaluar, String mensaje){
        if (evaluar<0) {
            System.out.println("no valido "+evaluar.getClass());
            throw new BadRequestException(mensaje);
        }
    }

    public static void validateDecimalPositive(BigDecimal evaluar, String mensaje){
        if (!(evaluar.compareTo(BigDecimal.ZERO) > 0) ) {
            System.out.println("no valido");
            throw new BadRequestException(mensaje);
        }
    }

    public static void validateNull(Object object, String mensaje){
        if (object==null) {
            System.out.println("no valido");
            throw new BadRequestException(mensaje);
        }
    }

}
