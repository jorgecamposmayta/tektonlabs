package org.tekton.labs.product.management.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.tekton.labs.product.management.model.entity.DetailProductCountryEntity;

import java.util.List;

@Repository
public interface DetailProductCountryJPARepository extends JpaRepository<DetailProductCountryEntity, Long> {

    @Query(value = "SELECT * FROM detail_product_country WHERE product_code = :product_code", nativeQuery = true)
    List<DetailProductCountryEntity> findAllByProductCode(@Param("product_code") Long product_code);

}
