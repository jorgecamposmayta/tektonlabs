package org.tekton.labs.product.management.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.tekton.labs.product.management.model.entity.CountryEntity;

@Repository
public interface CountryRepository extends CrudRepository<CountryEntity, Long> {

}
