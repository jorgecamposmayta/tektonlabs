package org.tekton.labs.product.management.model.api;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class DetailProductCountry {
    private Long code;
    private Long productCode;
    private Country country;
    private BigDecimal manufacturingCost;
    private BigDecimal priceWithoutTaxes;

}
