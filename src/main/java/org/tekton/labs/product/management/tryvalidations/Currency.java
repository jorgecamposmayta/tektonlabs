package org.tekton.labs.product.management.tryvalidations;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = CurrencyValidator.class)
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Currency {
    String message() default "Formato de moneda inválido";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
