package org.tekton.labs.product.management.business;

import org.springframework.cache.annotation.Cacheable;
import org.tekton.labs.product.management.model.api.ProductType;

public interface ProductTypeManagementService {
    ProductType create(ProductType productType);
    @Cacheable(value = "productTypes", key = "#productTypeId")
    ProductType  findById(Long productTypeId);
}
