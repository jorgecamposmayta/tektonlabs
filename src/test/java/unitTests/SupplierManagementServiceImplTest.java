package unitTests;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.tekton.labs.product.management.business.SupplierManagementService;
import org.tekton.labs.product.management.business.impl.SupplierManagementServiceImpl;
import org.tekton.labs.product.management.model.api.Supplier;
import org.tekton.labs.product.management.model.entity.SupplierEntity;
import org.tekton.labs.product.management.repository.SupplierRepository;
import org.tekton.labs.product.management.util.mapper.SupplierMapper;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SupplierManagementServiceImplTest {

    @Mock
    private SupplierRepository supplierRepository;

    @Mock
    private CacheManager cacheManager;

    @InjectMocks
    private SupplierManagementService supplierManagementService = new SupplierManagementServiceImpl();

    @Test
    void testCreate() {
        // Simular el comportamiento del repositorio al crear un proveedor
        Supplier supplier = Supplier.builder().build();
        supplier.setName("Example Supplier");
        supplier.setCode(1l);
        SupplierEntity supplierEntity = SupplierMapper.buildSupplierEntity(supplier);
        when(supplierRepository.save(any(SupplierEntity.class))).thenReturn(supplierEntity);

        // Llamar al método de la clase de servicio y verificar la respuesta
        Supplier createdSupplier = supplierManagementService.create(supplier);
        assertEquals(supplier, createdSupplier);
    }

    @Test
    @Cacheable(value = "suppliers", key = "#supplierId")
    void testFindById() {
        // ID de ejemplo para la prueba
        Long supplierId = 1L;

        // Simular el comportamiento del repositorio al buscar un proveedor por ID
        SupplierEntity supplierEntity = new SupplierEntity();
        supplierEntity.setCode(supplierId);
        supplierEntity.setName("Example Supplier");
        when(supplierRepository.findById(supplierId)).thenReturn(Optional.of(supplierEntity));

        // Llamar al método de la clase de servicio y verificar la respuesta
        Supplier foundSupplier = supplierManagementService.findById(supplierId);
        assertEquals(supplierEntity.getName(), foundSupplier.getName());
    }
}

