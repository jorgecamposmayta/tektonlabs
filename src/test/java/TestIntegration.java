import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.tekton.labs.StartApplication;
import org.tekton.labs.product.management.business.SupplierManagementService;
import org.tekton.labs.product.management.model.api.Supplier;
import org.tekton.labs.product.management.model.entity.SupplierEntity;
import org.tekton.labs.product.management.repository.SupplierRepository;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = StartApplication.class)
class ProductServiceIntegrationTest {
        @Autowired
        private SupplierManagementService supplierManagementService;

        @Autowired
        private SupplierRepository supplierRepository;

        @Test
        public void testCreateProductIntegration() {
            Supplier supplier = Supplier.builder()
                    .code(5l)
                    .name("LG")
                    .build();
            Supplier createdSupplier = supplierManagementService.create(supplier);

            SupplierEntity supplierProduct = supplierRepository.findById(createdSupplier.getCode()).orElse(null);

            assertEquals(createdSupplier.getCode(), supplierProduct.getCode());
        }
    }
