IF NOT EXISTS (SELECT 1 FROM sys.databases WHERE name = 'tektonlabs')
BEGIN
    CREATE DATABASE tektonlabs;
END

USE tektonlabs;

-- Tabla supplier
IF NOT EXISTS (SELECT 1 FROM sys.tables WHERE name = 'supplier')
BEGIN
    CREATE TABLE supplier (
        supplier_code INT IDENTITY(1,1) PRIMARY KEY,
        supplier_name NVARCHAR(255) NOT NULL
    );

    -- Agregar �ndice no agrupado en el campo supplier_name
    CREATE INDEX IX_supplier_name ON supplier(supplier_name);
END

-- Tabla product_type
IF NOT EXISTS (SELECT 1 FROM sys.tables WHERE name = 'product_type')
BEGIN
    CREATE TABLE product_type (
        product_type_code INT IDENTITY(1,1) PRIMARY KEY,
        product_type_name NVARCHAR(255) NOT NULL
    );

    -- Agregar �ndice no agrupado en el campo product_type_name
    CREATE INDEX IX_product_type_name ON product_type(product_type_name);
END

-- Tabla product_category
IF NOT EXISTS (SELECT 1 FROM sys.tables WHERE name = 'product_category')
BEGIN
    CREATE TABLE product_category (
        product_category_code INT IDENTITY(1,1) PRIMARY KEY,
        product_category_name NVARCHAR(255) NOT NULL
    );

    -- Agregar �ndice no agrupado en el campo product_category_name
    CREATE INDEX IX_product_category_name ON product_category(product_category_name);
END

-- Tabla product
IF NOT EXISTS (SELECT 1 FROM sys.tables WHERE name = 'product')
BEGIN
    CREATE TABLE product (
        product_code INT IDENTITY(1,1) PRIMARY KEY,
        product_name NVARCHAR(255) NOT NULL,
        product_description NVARCHAR(1000),
        stock INT NOT NULL,
		rotation DECIMAL(10, 2) NOT NULL,
		supplier_code INT,
		product_type_code INT,
		product_category_code INT,
		FOREIGN KEY (supplier_code) REFERENCES supplier(supplier_code),
		FOREIGN KEY (product_type_code) REFERENCES product_type(product_type_code),
		FOREIGN KEY (product_category_code) REFERENCES product_category(product_category_code)
    );

    -- Agregar �ndice no agrupado en el campo product_name
    CREATE INDEX IX_product_name ON product(product_name);
END

-- Tabla country
IF NOT EXISTS (SELECT 1 FROM sys.tables WHERE name = 'country')
BEGIN
    CREATE TABLE country (
        country_code INT IDENTITY(1,1) PRIMARY KEY,
        country_name NVARCHAR(255) NOT NULL,
		currency NVARCHAR(3) NOT NULL
    );

    -- Agregar �ndice no agrupado en el campo country_name
    CREATE INDEX IX_country_name ON country(country_name);
END


-- Tabla detail_product_country (Tabla de muchos a muchos)
IF NOT EXISTS (SELECT 1 FROM sys.tables WHERE name = 'detail_product_country')
BEGIN
    CREATE TABLE detail_product_country (
        detail_product_country_code INT IDENTITY(1,1) PRIMARY KEY,
        product_code INT,
        country_code INT,
        manufacturing_cost DECIMAL(10, 2) NOT NULL,
		price_without_taxes DECIMAL(10, 2) NOT NULL,
        FOREIGN KEY (product_code) REFERENCES product(product_code),
		FOREIGN KEY (country_code) REFERENCES country(country_code)
    );

    -- Agregar �ndice no agrupado en los campos product_code y country_code
    CREATE INDEX IX_detail_product_country_Product_code_Country_code ON detail_product_country(product_code, country_code);
END



  -- Insertar valores para Estados Unidos (USD)
INSERT INTO [dbo].[country] ([country_name], [currency])
VALUES ('Estados Unidos', 'USD');

-- Insertar valores para Per� (PEN)
INSERT INTO [dbo].[country] ([country_name], [currency])
VALUES ('Per�', 'PEN');

-- Insertar valores para Espa�a (EUR)
INSERT INTO [dbo].[country] ([country_name], [currency])
VALUES ('Espa�a', 'EUR');

-- Insertar valores en la tabla [dbo].[supplier]
INSERT INTO [dbo].[supplier] ([supplier_name])
VALUES
('LG'),
('SANSUMG'),

('SPY'),
('OEM'),

('CASA JOVEN'),
('MI CASA'),

('CHANNEL'),
('DIOR'),
('HERBAL ESCENCE'),

('PANTENE'),
('SANSUMG');


INSERT INTO [tektonlabs].[dbo].[product_type] ([product_type_name])
VALUES
('TV'),
('ALARMAS'),
('CAMARAS'),
('MESAS'),
('LAVADORAS'),
('BELLEZA');

INSERT INTO [tektonlabs].[dbo].[product_category] ([product_category_name])
VALUES
('BELLEZA Y SALUD'),
('ELECTRODOMESTICO'),
('MUEBLES'),
('SEGURIDAD');
